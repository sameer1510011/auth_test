from django.conf.urls import patterns, include, url
from django.contrib import admin
from account import views

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'HealthBrio.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^signup/$', views.SignUp.as_view(), name="sign_up"),
    url(r'^login/$', views.Login.as_view(), name="login"),
    url(r'^(?P<username>\w+)/(?P<key_data>\w+)/confirm/$', views.email_confirm),
    )