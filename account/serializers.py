from rest_framework import serializers
from oauth2_provider.models import *
from .models import *
import views 


class SignUpSerializer(serializers.ModelSerializer):

    class Meta:
        model = Profile
        fields = ('username','email','password', 'first_name','last_name',)
        write_only_fields = ('password',)




class LoginSerializer(serializers.ModelSerializer):

    info = serializers.SerializerMethodField()
    token = serializers.SerializerMethodField()
    class Meta:
        model = Profile
        fields = ('token', 'info',)       


    def get_token(self, obj):
        """
        Get Acess token
        """
        print "Login serializer"
        print "EMAIL"
        print obj         
        return "fsdfs"
#        return views.get_access_token(obj)



    def get_info(self, obj):
        print "get in info"
        print obj.email
        print obj.username
        print type(obj)

        
        profile=Profile.objects.get(email=obj.email)
        print type(profile)
        #application=Application.objects.get(user =obj)
        # for i in application:
        #     print i.user

        return "hfhf"
#        return {'profile_id': profile.id ,'client_id':application.client_id, 'client_secret': application.client_secret }






# class AccountSerializer(SignUpSerializer):
#     """
#     Account Related information

#     """
#     class Meta:
#         model = Account
#         fields = ('email', 'username','first_name','middle_name','last_name','mobile','is_active',)       




# class PatientSerializer(serializers.ModelSerializer):
#     """
#     Patient serializer to get Patient information
#     """
#     owner=AccountSerializer()

#     class Meta:
#         model=Patient        
#         fields=('owner','extra')



# class PatientConditionSerializer(serializers.ModelSerializer):
#     """
#     Patient Condition serializer 
#     """
#     patient = serializers.ReadOnlyField(source='patient.owner.username')

#     class Meta:
#         model=PatientCondition        
#         fields=('id','patient','condition','date','doctor','note',)


# class PatientProcedureSerializer(serializers.ModelSerializer):
#     """
#      Patient Procedure serializer 
#     """
#     patient = serializers.ReadOnlyField(source='patient.owner.username')

#     class Meta:
#         model=PatientProcedure        
#         fields=('id','patient','procedure','date','doctor','note',)


# class PatientAllergySerializer(serializers.ModelSerializer):
#     """
#      Patient Allergy serializer 
#     """
#     patient = serializers.ReadOnlyField(source='patient.owner.username')

#     class Meta:
#         model=PatientAllergy        
#         fields=('id','patient','reaction','name','date','note',)


# class UnitSerializer(serializers.ModelSerializer):
#     """
#      Unit serializer 
#     """
#     class Meta:
#         model=Unit        
#         fields=('id','name',)


# class MethodSerializer(serializers.ModelSerializer):
#     """
#      Method serializer 
#     """
#     class Meta:
#         model=Method        
#         fields=('id','name',)




# class PatientMedicationSerializer(serializers.ModelSerializer):
#     """
#      Patient Medication serializer 
#     """
#     patient = serializers.ReadOnlyField(source='patient.owner.username')

#     class Meta:
#         model=PatientMedication        
#         fields=('id','patient','salt','amount','unit','method','date','note','doctor',)






# class PatientMyProfileSerializer(serializers.ModelSerializer):
#     """
#      Patient Profile and family member informaiton serializer 
#     """
#     patient = serializers.ReadOnlyField(source='patient.owner.username')
#     class Meta:
#         model=MyProfile        
#         fields=('id','patient','first_name','last_name','gender','date','email','zipcode','is_super','mobile',)
#         read_only_fields = ('is_super',)