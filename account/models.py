from django.contrib.auth.models import AbstractBaseUser
from django.db import models
from django.contrib.auth.models import *
from django.contrib.auth import *
from Crypto.Cipher import AES
import binascii
from oauth2_provider.models import Application
from django.db import models



class AccountManager(BaseUserManager):
	def create_user(self, email, password=None, **kwargs):
		if not email:
			raise ValueError('Users must have a valid email address.')


		if not kwargs.get('username'):
			raise ValueError('Users must have a valid username.')

		profile = self.model(
			email=self.normalize_email(email), username=kwargs.get('username')
		)

		profile.set_password(password)
		profile.save()
		Application.objects.create(user=profile, client_type=Application.CLIENT_CONFIDENTIAL,authorization_grant_type=Application.GRANT_PASSWORD)
		return profile

	def create_superuser(self, email, password, **kwargs):
		profile = self.create_user(email, password, **kwargs)

		profile.is_admin = True
		profile.is_staff= True
		profile.is_superuser=True
		profile.is_active=True
		profile.save()

		return profile


class Profile(AbstractBaseUser,PermissionsMixin):

    email = models.EmailField(unique=True)
    username = models.CharField(max_length=90, unique=True)
    first_name = models.CharField(max_length=90, blank=True,null=True)
    last_name = models.CharField(max_length=90, blank=True,null=True)

    key=models.CharField(max_length = 128,blank=True,null=True)

    is_admin = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)

    objects = AccountManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    def __unicode__(self):
        return self.email

    def get_short_name(self):
        return self.first_name