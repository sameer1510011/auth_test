from django.shortcuts import render
from .models import *
from rest_framework import generics
# Create your views here.
from .tasks import *
from oauth2_provider.settings import oauth2_settings
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
import string
from string import strip 
import random
import urllib3
from django.views.generic import View
from django.http import HttpResponse
from rest_framework.authentication import *
from oauth2_provider.models import *
from .serializers import *
from auth_test.static_values import *


def get_token_json(acces_token):

    token = {
        'acces_token' : acces_token.token,
        'expires_in' : acces_token.expires,
        'refresh_token' : acces_token.refresh_token.token,
        'scope' : acces_token.scope
    }

    return token

def get_access_token(user):

    print "token view1"
    print user.email
    app = Application.objects.get(user=user)
    print "token view2"
    token = generat_token()

    refresh_token = generat_token()

    expires = now()+timedelta(seconds=oauth2_settings.ACCESS_TOKEN_EXPIRE_SECONDS)

    scope = "read write"

    acces_token = AccesToken.objects.\
        create(user = user,
                application = app,
                expires = expires,
                token = token,
                scope = scope)

    RefreshToken.objects.\
        create(user = user,
                application = app,
                token = refresh_token,
                acces_token = acces_token)

    return get_token_json(acces_token)




def email_verification(profile):
    try:
        profile=Profile.objects.get(username=profile.username)
        profile.key=str(random.randint(100,999)) 
        profile.user_joining_time=timezone.now()
        profile.save()

        key =settings.KEY_ID
        username_plaintext =profile.username
        length = 16 - (len(username_plaintext) % 16)
        username_plaintext += ("#")*length

        encobj = AES.new(key, AES.MODE_ECB)
        username_ciphertext = encobj.encrypt(username_plaintext)

        key_plaintext =profile.key
        length = 16 - (len(key_plaintext) % 16)
        key_plaintext += ("#")*length
        key_encobj = AES.new(key, AES.MODE_ECB)
        key_ciphertext = key_encobj.encrypt(key_plaintext)

        email_subject = 'Client Email Varification'
        print "DOMAIN_NAME"
        print DOMAIN_NAME

        email_body = "Hey,thanks for signing up. To activate your account, click this link "+DOMAIN_NAME+"api/"+username_ciphertext.encode('hex')+"/"+key_ciphertext.encode('hex')+"/confirm/"

        print "hih hihi"
        sendmail(email_subject,email_body,[profile.email])

    except Exception, e:
        print e



class SignUp(generics.CreateAPIView):
    queryset = Profile.objects.all()
    serializer_class = SignUpSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)

        try:
            instance=Profile.objects.get(email=request.data['email'])
            instance.set_password(request.data['password'])
            instance.signup_source_local=True
            instance.save()
            email_verification(instance)
            Application.objects.create(user=instance,client_type=Application.CLIENT_CONFIDENTIAL,authorization_grant_type=Application.GRANT_PASSWORD)

            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

        except Exception,e:
            print e
            return Response(status=HTTP_400_BAD_REQUEST)


def email_confirm(request,username,key_data):

    try:
        key = settings.KEY_ID
        user_ciphertext = binascii.unhexlify(username)
        decobj = AES.new(key, AES.MODE_ECB)
        user_plaintext = decobj.decrypt(user_ciphertext)
        print "try try"
        key_ciphertext = binascii.unhexlify(key_data)
        decobj = AES.new(key, AES.MODE_ECB)
        key_plaintext = decobj.decrypt(key_ciphertext)
        print "query start"
        u = Profile.objects.get(username=(user_plaintext.strip('#')))
        print "query end"
        if int(u.key)==int(key_plaintext.strip('#')):
        	u.is_active=True
        	u.save()

        	return HttpResponse('Done')

        else:
        	return HttpResponse('Error')
    
    except Exception ,e:
    	return HttpResponse(e)


class Login(generics.ListAPIView):
    queryset = Profile.objects.all()
    serializer_class = LoginSerializer
    print "Login View"
    authentication_class = (BasicAuthentication,)


    def get_querset(self):
        return [self.request.user]